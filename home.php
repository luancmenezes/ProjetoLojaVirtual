<?php
  session_start();
  if(!$_SESSION['sessao_codigo_usuario']){
   require('util/funcoes.php');
   direciona('index.php');
   exit;
  }
  else {
  require('util/conecta.php');
  require('entidadeDAO/menu_DAO.php');

  $oquefazer = new menu_DAO();
  $submenu_vertical = new menu_DAO();
  $submenu_horizontal = new menu_DAO();
?>
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <?php require('util/head.php');?>
  </head>
  <body>
    <?php
    $id = $_REQUEST['id'];
    $codmenu_vertical =  $_REQUEST['codmenu_vertical'];
    $codmenu_horizontal =  $_REQUEST['codmenu_horizontal'];
    $acao   = $_REQUEST['acao'];
    $desc   = $_REQUEST['desc'];
    ?>

<!-- ////////////////////////CABECALHO//////////////////////////////////////////////////// -->  

<div class="container">
  <div class="row">
    <header class="cabecalho span12">
      <h1 align="center">BEM VINDOS AO SITE DE VENDAS</h1>
       <a href="admin/index.php">Configuração</a><br>
      <a href="admin/login/logoff.php">Sair</a>
    </header>
  </div>
</div>

<!-- //////////////////MENU HORIZONTAL////////////////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <nav class="navbar span12 MenuHorizontal">
      <div class="container">
        <button class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="nav-collapse collapse"><?php require('menu_horizontal.php');?></div>
      </div>
    </nav>
  </div>
</div>

<!-- ////////////////////////MENU VERTICAL DIREITO//////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <section class="span2 ladoesquerdo"><?php require('menu_vertical.php');?></section>

<!-- /////////////////////////CORPO DA PAGINA- CENTRO////////////////////////////////////// -->

    <section class="span8 centro" id="btn-dropdown">
      <header class="page-header">
        <h1><?php echo $desc;?></h1>
      </header>
      <?php 
      if($id == 1)
        require('controller/produto_acao.php');
      ?>
    </section>

<!-- /////////////////////////MENU VERTICAL ESQUERDO/////////////////////////////////////// -->
    
    <section class="span2 ladodireito">
      <!--<a href="admin/index.php">Configuração</a><br> -->
      <!--<a href="admin/login/logoff.php">Sair</a> -->
    </section>
  </div>
</div>

<!-- //////////////////////////RODAPE////////////////////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <footer class="footer span12">
      <h3 align="center">INFORMACOES EXTRAS DA EMPRESA</h3>
    </footer>
  </div>
</div>

<!-- ////////////////////////////////////////////////////////////////////////////////////// -->

  </body>
</html>   
<?php
  }
?>
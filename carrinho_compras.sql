-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 19-Set-2015 às 16:05
-- Versão do servidor: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `carrinho_compras`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_carrinho`
--

CREATE TABLE IF NOT EXISTS `tbl_carrinho` (
  `PROD_CODIGO` int(11) NOT NULL,
  `CAR_SESSAO` varchar(60) NOT NULL,
  `CAR_QUANTIDADE` int(11) DEFAULT NULL,
  `CAR_VALOR` decimal(15,2) DEFAULT NULL,
  `CAR_DATA` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_carrinho`
--

INSERT INTO `tbl_carrinho` (`PROD_CODIGO`, `CAR_SESSAO`, `CAR_QUANTIDADE`, `CAR_VALOR`, `CAR_DATA`) VALUES
(13, 'o9143k2uji5v3i5mo8l7hvu8r1', 1, 119.00, '0000-00-00'),
(6, 'o9143k2uji5v3i5mo8l7hvu8r1', 1, 99.00, '0000-00-00'),
(9, 'o9143k2uji5v3i5mo8l7hvu8r1', 1, 139.00, '0000-00-00'),
(17, 'o9143k2uji5v3i5mo8l7hvu8r1', 1, 99.00, '0000-00-00'),
(12, 'o9143k2uji5v3i5mo8l7hvu8r1', 1, 119.00, '0000-00-00'),
(6, '71pcp37k572jpds097ji3u0951', 1, 99.00, '0000-00-00'),
(11, '71pcp37k572jpds097ji3u0951', 1, 99.00, '0000-00-00'),
(6, '465ps27k842tnkkq8njegc1e60', 1, 99.00, '0000-00-00'),
(9, '465ps27k842tnkkq8njegc1e60', 1, 139.00, '0000-00-00'),
(5, '465ps27k842tnkkq8njegc1e60', 1, 99.00, '0000-00-00'),
(7, '465ps27k842tnkkq8njegc1e60', 1, 109.00, '0000-00-00'),
(7, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 109.00, '0000-00-00'),
(10, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 99.00, '0000-00-00'),
(13, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 119.00, '0000-00-00'),
(14, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 129.00, '0000-00-00'),
(15, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 114.00, '0000-00-00'),
(8, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 139.00, '0000-00-00'),
(17, 'l86ul7u1s9v0pc59m0ti1gl0m4', 1, 99.00, '0000-00-00'),
(14, 'i6jar3pq7fm6ldha3nqetmugu5', 1, 129.00, '0000-00-00'),
(7, 'i6jar3pq7fm6ldha3nqetmugu5', 1, 109.00, '0000-00-00'),
(5, 'i6jar3pq7fm6ldha3nqetmugu5', 1, 99.00, '0000-00-00'),
(12, 'i6jar3pq7fm6ldha3nqetmugu5', 1, 119.00, '0000-00-00'),
(15, 'i6jar3pq7fm6ldha3nqetmugu5', 1, 114.00, '0000-00-00'),
(6, 'i6jar3pq7fm6ldha3nqetmugu5', 1, 99.00, '0000-00-00'),
(13, 's2u0eqi5vinojic21ng32642g4', 1, 119.00, '0000-00-00'),
(5, '5bgcirnbfs42pk6sh5g1nvbjm5', 1, 99.00, '0000-00-00'),
(8, 's2u0eqi5vinojic21ng32642g4', 2, 139.00, '0000-00-00'),
(15, 's2u0eqi5vinojic21ng32642g4', 1, 114.00, '0000-00-00'),
(2, 's2u0eqi5vinojic21ng32642g4', 3, 109.00, '0000-00-00'),
(6, 's2u0eqi5vinojic21ng32642g4', 1, 99.00, '0000-00-00'),
(2, 'bp6ku1pc6sea4te6i7h4pp1683', 1, 109.00, '0000-00-00'),
(7, 'bp6ku1pc6sea4te6i7h4pp1683', 1, 109.00, '0000-00-00'),
(6, 'bp6ku1pc6sea4te6i7h4pp1683', 1, 99.00, '0000-00-00'),
(17, 'qr49ofk57g615ds68ib6q1lfd5', 1, 99.00, '0000-00-00'),
(9, 'u679d537ru8v0b8gq9vfu411i7', 1, 139.00, '0000-00-00'),
(14, 'u679d537ru8v0b8gq9vfu411i7', 2, 129.00, '0000-00-00'),
(6, 'u679d537ru8v0b8gq9vfu411i7', 1, 99.00, '0000-00-00'),
(10, 'u679d537ru8v0b8gq9vfu411i7', 1, 99.00, '0000-00-00'),
(15, 'u679d537ru8v0b8gq9vfu411i7', 1, 114.00, '0000-00-00'),
(16, 'u679d537ru8v0b8gq9vfu411i7', 1, 114.00, '0000-00-00'),
(8, '3minavh9htbh4pbigt9q9cmhs3', 1, 139.00, '2010-01-16'),
(10, '3minavh9htbh4pbigt9q9cmhs3', 2, 99.00, '2010-01-16'),
(9, '3minavh9htbh4pbigt9q9cmhs3', 1, 139.00, '2010-01-16'),
(11, '3minavh9htbh4pbigt9q9cmhs3', 1, 99.00, '0000-00-00'),
(16, '3minavh9htbh4pbigt9q9cmhs3', 1, 114.00, '2010-01-16'),
(13, '3minavh9htbh4pbigt9q9cmhs3', 1, 119.00, '2010-01-16'),
(2, 'u997ql95henaue1rme7em40t44', 1, 109.00, '2010-01-16'),
(9, 'u997ql95henaue1rme7em40t44', 1, 139.00, '2010-01-16'),
(4, 'vh0ahf8ptipdlm4k9glsvvsmr0', 1, 99.00, '2010-01-18'),
(2, 'vh0ahf8ptipdlm4k9glsvvsmr0', 1, 109.00, '2010-01-18'),
(6, 'vh0ahf8ptipdlm4k9glsvvsmr0', 1, 99.00, '2010-01-18'),
(14, 'nulbpb47r6rh3fnpk37746inf5', 1, 129.00, '2010-01-20'),
(7, '874a7bmqao1va3a43bn4249hm2', 1, 109.00, '2015-08-22'),
(8, '874a7bmqao1va3a43bn4249hm2', 1, 139.00, '2015-08-22'),
(5, 'c4sq4kptobuo27fk43g715vd34', 1, 99.00, '2015-08-22'),
(5, '874a7bmqao1va3a43bn4249hm2', 1, 99.00, '2015-08-22'),
(12, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 119.00, '2015-09-02'),
(2, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 109.00, '2015-09-02'),
(6, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 99.00, '2015-09-01'),
(7, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 109.00, '2015-09-02'),
(9, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 139.00, '2015-09-02'),
(4, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 99.00, '2015-09-01'),
(5, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 99.00, '2015-09-01'),
(5, '1j0i43psifqikapgv6724mh9m0', 1, 99.00, '2015-09-01'),
(4, 'rbp8vrbda4nu2o43gp0uirc716', 1, 99.00, '2015-08-31'),
(11, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 99.00, '2015-09-02'),
(10, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 99.00, '2015-09-02'),
(17, 'fl23mch1eopv9q5c1b7ita2ht1', 1, 99.00, '2015-09-02'),
(5, 'ae7u9jrerhgvo3h7ntagup37o6', 1, 99.00, '2015-09-18'),
(5, 'cf61q97mqtkputmncrhde3mua1', 1, 99.00, '2015-09-15'),
(5, 'cq6mcu70c3rr647k05htnsuvj0', 1, 99.00, '2015-09-11'),
(4, 'cq6mcu70c3rr647k05htnsuvj0', 1, 99.00, '2015-09-11'),
(4, 'n96sb927k59efumrab65m01e46', 1, 99.00, '2015-09-03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cidade`
--

CREATE TABLE IF NOT EXISTS `tbl_cidade` (
  `CID_CODIGO` int(11) NOT NULL,
  `CID_DESCRICAO` varchar(40) NOT NULL,
  `CID_UF` char(2) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_cidade`
--

INSERT INTO `tbl_cidade` (`CID_CODIGO`, `CID_DESCRICAO`, `CID_UF`) VALUES
(5, 'Guaruja do Sul', 'SP'),
(7, 'Gurupi', 'TO'),
(28, 'Sarandi', 'RS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cliente`
--

CREATE TABLE IF NOT EXISTS `tbl_cliente` (
  `CLI_CODIGO` int(11) NOT NULL,
  `CID_CODIGO` int(11) NOT NULL,
  `CLI_NOME` varchar(40) NOT NULL,
  `CLI_ENDERECO` varchar(50) DEFAULT NULL,
  `CLI_NUMERO` varchar(10) DEFAULT NULL,
  `CLI_COMPLEMENTO` varchar(20) DEFAULT NULL,
  `CLI_BAIRRO` varchar(30) DEFAULT NULL,
  `CLI_CIDADE` varchar(30) NOT NULL,
  `CLI_CEP` char(10) DEFAULT NULL,
  `CLI_FONERES` varchar(16) DEFAULT NULL,
  `CLI_FONECEL` varchar(16) DEFAULT NULL,
  `CLI_FONECOM` varchar(16) DEFAULT NULL,
  `CLI_CPF` varchar(14) DEFAULT NULL,
  `CLI_RG` varchar(20) DEFAULT NULL,
  `CLI_DATACADASTRO` date DEFAULT NULL,
  `CLI_DATANASC` date DEFAULT NULL,
  `CLI_EMAIL` varchar(60) DEFAULT NULL,
  `CLI_LOGIN` varchar(10) DEFAULT NULL,
  `CLI_SENHA` varchar(10) DEFAULT NULL,
  `CLI_DATAULTCOMP` date DEFAULT NULL,
  `CLI_VALOR_ULTCOMP` decimal(15,2) DEFAULT NULL,
  `CLI_VALOR_TOTAL` decimal(15,2) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_cliente`
--

INSERT INTO `tbl_cliente` (`CLI_CODIGO`, `CID_CODIGO`, `CLI_NOME`, `CLI_ENDERECO`, `CLI_NUMERO`, `CLI_COMPLEMENTO`, `CLI_BAIRRO`, `CLI_CIDADE`, `CLI_CEP`, `CLI_FONERES`, `CLI_FONECEL`, `CLI_FONECOM`, `CLI_CPF`, `CLI_RG`, `CLI_DATACADASTRO`, `CLI_DATANASC`, `CLI_EMAIL`, `CLI_LOGIN`, `CLI_SENHA`, `CLI_DATAULTCOMP`, `CLI_VALOR_ULTCOMP`, `CLI_VALOR_TOTAL`) VALUES
(1, 6, 'Jailton Pereira', 'Rua da Céu Sn', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1111-11-11', '1111-11-11', '1', '1', '1', '1111-11-11', 1.00, 1.00),
(2, 34, 'jailton', 'rua do ceu', '245', 'casa', 'ilhota-', '0', '44470000', '8888888', '99999999', '12345678', '98765432135', '98765432112', '0000-00-00', '0000-00-00', 'itops@hotmail.com', 'jailton', '123', '0000-00-00', 50.00, 50.00),
(3, 5, 'santos', 'santos', '3425', '1', '1', '0', '1', '1', '1', '1', '1', '1', '0000-00-00', '0000-00-00', '1', '1', '1', '0000-00-00', 1.00, 1.00),
(4, 5, 'santos', 'santos', '2', '2', '2', '0', '2', '2', '2', '2', '2', '2', '0000-00-00', '0000-00-00', '2', '2', '2', '0000-00-00', 2.00, 2.00);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_fornecedor`
--

CREATE TABLE IF NOT EXISTS `tbl_fornecedor` (
  `FOR_CODIGO` int(11) NOT NULL,
  `CID_CODIGO` int(11) NOT NULL,
  `FOR_RAZAOSOCIAL` varchar(40) NOT NULL,
  `FOR_NOME_FANTASIA` varchar(40) DEFAULT NULL,
  `FOR_ENDERECO` varchar(50) DEFAULT NULL,
  `FOR_BAIRRO` varchar(30) DEFAULT NULL,
  `FOR_FONE` varchar(16) DEFAULT NULL,
  `FOR_RESPONSAVEL` varchar(50) DEFAULT NULL,
  `FOR_EMAIL` varchar(60) DEFAULT NULL,
  `FOR_CNPJ` varchar(18) DEFAULT NULL,
  `FOR_CEP` varchar(10) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_fornecedor`
--

INSERT INTO `tbl_fornecedor` (`FOR_CODIGO`, `CID_CODIGO`, `FOR_RAZAOSOCIAL`, `FOR_NOME_FANTASIA`, `FOR_ENDERECO`, `FOR_BAIRRO`, `FOR_FONE`, `FOR_RESPONSAVEL`, `FOR_EMAIL`, `FOR_CNPJ`, `FOR_CEP`) VALUES
(4, 28, 'Universidade Luterana', 'Ulbra', 'Br', 'mato', '54 3329 1111', 'Valdemar', 'ulbra@ulbra.br', '1321321', '99500-000'),
(3, 7, 'Empresa teste', 'empresa', 'Av Flores da Cunha', 'centro', '54 3330 1222', 'Neri', 'itops@hotmail.com', '321654', '99500-000');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_imagem`
--

CREATE TABLE IF NOT EXISTS `tbl_imagem` (
  `IMG_CODIGO` int(11) NOT NULL,
  `PROD_CODIGO` int(11) NOT NULL,
  `IMG_DESCRICAO` text
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_imagem`
--

INSERT INTO `tbl_imagem` (`IMG_CODIGO`, `PROD_CODIGO`, `IMG_DESCRICAO`) VALUES
(31, 9, 'javaee.jpg'),
(29, 8, 'delphiIII.jpg'),
(28, 7, 'delphi7.jpg'),
(30, 2, 'javaseII.jpg'),
(25, 5, 'delphi.jpg'),
(27, 6, 'php.gif'),
(26, 4, 'java.jpg'),
(32, 10, 'javameI.jpg'),
(33, 11, 'j2meII.jpg'),
(34, 12, 'hibernate.JPG'),
(35, 13, 'banco1.jpg'),
(36, 14, 'oracle.jpg'),
(37, 15, 'Flex1.jpg'),
(38, 16, 'Flex2.jpg'),
(53, 6, 'Sem tÃ­tulo 2.png'),
(49, 19, 'Sem tÃ­tulo 2.png'),
(50, 20, 'Sem tÃ­tulo 2.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_itens_pedido`
--

CREATE TABLE IF NOT EXISTS `tbl_itens_pedido` (
  `PROD_CODIGO` int(11) NOT NULL,
  `PED_CODIGO` int(11) NOT NULL,
  `PED_QUANT` int(11) DEFAULT NULL,
  `PED_VALOR` decimal(15,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_itens_pedido`
--

INSERT INTO `tbl_itens_pedido` (`PROD_CODIGO`, `PED_CODIGO`, `PED_QUANT`, `PED_VALOR`) VALUES
(5, 12, 1, 99.00),
(15, 11, 1, 114.00),
(5, 11, 1, 99.00),
(13, 11, 1, 119.00),
(13, 12, 1, 119.00),
(17, 13, 1, 99.00),
(5, 13, 1, 99.00),
(14, 14, 1, 129.00),
(13, 14, 2, 119.00),
(14, 15, 1, 129.00),
(9, 16, 1, 139.00),
(14, 16, 1, 129.00),
(7, 17, 1, 109.00),
(13, 18, 1, 119.00),
(14, 18, 1, 129.00),
(13, 19, 1, 119.00),
(14, 19, 1, 129.00),
(6, 20, 1, 99.00),
(11, 21, 1, 99.00),
(14, 21, 1, 129.00),
(6, 22, 1, 99.00),
(2, 22, 1, 109.00),
(13, 23, 1, 119.00),
(8, 23, 1, 139.00),
(14, 24, 1, 129.00),
(2, 25, 2, 109.00),
(5, 25, 3, 99.00),
(2, 26, 1, 109.00),
(5, 26, 1, 99.00),
(5, 27, 1, 99.00),
(4, 28, 1, 99.00),
(5, 29, 1, 99.00),
(6, 30, 1, 99.00),
(5, 31, 1, 99.00),
(5, 32, 1, 99.00),
(5, 33, 1, 99.00),
(5, 34, 1, 99.00),
(5, 35, 1, 99.00),
(4, 36, 1, 99.00),
(6, 37, 1, 99.00),
(5, 38, 1, 99.00),
(4, 39, 1, 99.00),
(4, 40, 1, 99.00),
(4, 41, 1, 99.00),
(5, 42, 1, 99.00),
(7, 43, 1, 109.00),
(5, 43, 1, 99.00),
(4, 44, 1, 99.00),
(14, 45, 1, 129.00),
(4, 46, 1, 99.00),
(5, 47, 1, 99.00),
(5, 48, 1, 99.00),
(4, 49, 1, 99.00),
(5, 50, 1, 99.00),
(5, 51, 1, 99.00),
(4, 52, 1, 99.00),
(2, 53, 2, 109.00),
(4, 53, 5, 99.00),
(4, 54, 3, 99.00),
(6, 54, 2, 99.00),
(4, 55, 8, 99.00),
(2, 56, 6, 109.00),
(14, 57, 7, 129.00),
(18, 58, 1, 987.00),
(14, 58, 1, 129.00),
(4, 59, 7, 99.00),
(6, 60, 1, 99.00),
(14, 60, 1, 129.00),
(5, 61, 1, 99.00),
(14, 62, 1, 129.00),
(15, 63, 1, 114.00),
(5, 63, 1, 99.00),
(12, 64, 1, 119.00),
(7, 65, 1, 109.00),
(5, 65, 1, 99.00),
(8, 65, 1, 139.00),
(17, 65, 1, 99.00),
(12, 65, 1, 119.00),
(10, 65, 1, 99.00),
(12, 66, 1, 119.00),
(6, 66, 1, 99.00),
(17, 66, 1, 99.00),
(8, 66, 1, 139.00),
(5, 66, 1, 99.00),
(4, 66, 1, 99.00),
(2, 66, 1, 109.00),
(11, 66, 1, 99.00),
(10, 66, 8, 99.00),
(8, 67, 1, 139.00),
(2, 67, 1, 109.00),
(5, 67, 1, 99.00),
(7, 67, 8, 109.00),
(17, 67, 1, 99.00),
(10, 67, 1, 99.00),
(15, 68, 1, 114.00),
(6, 68, 1, 99.00),
(17, 68, 1, 99.00),
(2, 68, 1, 109.00),
(4, 68, 1, 99.00),
(4, 69, 1, 99.00),
(7, 70, 1, 109.00),
(7, 71, 1, 109.00),
(5, 71, 1, 99.00),
(6, 72, 1, 99.00),
(5, 72, 1, 99.00),
(5, 73, 1, 99.00),
(6, 74, 1, 99.00),
(5, 75, 1, 99.00),
(5, 76, 1, 99.00),
(5, 77, 1, 99.00),
(5, 78, 1, 99.00),
(5, 79, 1, 99.00),
(5, 80, 1, 99.00),
(5, 81, 1, 99.00),
(5, 82, 1, 99.00),
(6, 83, 1, 99.00),
(7, 83, 1, 109.00),
(5, 84, 1, 99.00),
(7, 85, 1, 109.00),
(5, 86, 1, 99.00),
(5, 87, 1, 99.00),
(6, 87, 1, 99.00),
(7, 87, 1, 109.00),
(5, 88, 1, 99.00),
(4, 89, 1, 99.00),
(16, 90, 1, 114.00),
(5, 90, 1, 99.00),
(17, 90, 1, 99.00),
(9, 91, 1, 139.00),
(4, 91, 1, 99.00),
(5, 92, 1, 99.00),
(7, 92, 1, 109.00),
(8, 92, 1, 139.00),
(15, 92, 1, 114.00),
(4, 93, 1, 99.00),
(8, 93, 1, 139.00),
(5, 94, 1, 99.00),
(5, 95, 1, 99.00),
(5, 96, 1, 99.00),
(4, 97, 1, 99.00),
(2, 98, 1, 109.00),
(5, 98, 1, 99.00),
(4, 99, 1, 99.00),
(7, 99, 1, 109.00),
(8, 99, 1, 139.00),
(15, 99, 1, 114.00),
(6, 99, 1, 99.00),
(17, 99, 1, 99.00),
(5, 99, 1, 99.00),
(5, 100, 1, 99.00),
(5, 101, 1, 99.00),
(5, 102, 1, 99.00),
(5, 103, 1, 99.00),
(5, 104, 1, 99.00),
(5, 105, 90, 99.00),
(17, 106, 2, 99.00),
(6, 106, 3, 99.00),
(14, 106, 1, 129.00),
(5, 107, 1, 99.00),
(4, 108, 1, 99.00);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_menu_horizontal`
--

CREATE TABLE IF NOT EXISTS `tbl_menu_horizontal` (
  `MENUHOR_CODIGO` int(11) NOT NULL,
  `MENUHOR_DESCRICAO` varchar(30) NOT NULL,
  `MENUHOR_CODEMP` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_menu_horizontal`
--

INSERT INTO `tbl_menu_horizontal` (`MENUHOR_CODIGO`, `MENUHOR_DESCRICAO`, `MENUHOR_CODEMP`) VALUES
(1, 'Bootstrap Twitter', 0),
(2, 'Wordpress', 0),
(3, 'Javascript', 0),
(4, 'Jquery', 0),
(5, 'jailton', 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_menu_vertical`
--

CREATE TABLE IF NOT EXISTS `tbl_menu_vertical` (
  `CAT_CODIGO` int(11) NOT NULL,
  `CAT_DESCRICAO` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_menu_vertical`
--

INSERT INTO `tbl_menu_vertical` (`CAT_CODIGO`, `CAT_DESCRICAO`) VALUES
(4, 'Delphi'),
(2, 'Java'),
(3, 'Php'),
(5, 'Banco de Dados'),
(6, 'Flex'),
(7, 'Algoritmos e LÃ³gica'),
(8, 'Moodle'),
(9, 'Windows e Office'),
(10, 'SolidWorks'),
(11, 'Html Javascript CSS'),
(12, 'Quest3d'),
(13, 'Excel AvanÃ§ado'),
(14, 'Fireworks'),
(32, 'jailton');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_pedido`
--

CREATE TABLE IF NOT EXISTS `tbl_pedido` (
  `PED_CODIGO` int(11) NOT NULL,
  `CLI_CODIGO` int(11) NOT NULL,
  `PED_DATA` date DEFAULT NULL,
  `PED_HORA` time DEFAULT NULL,
  `PED_BOLETO` text,
  `PED_VALORTOTAL` decimal(15,2) DEFAULT NULL,
  `PED_VALORFRETE` decimal(15,2) DEFAULT NULL,
  `PED_STATUS` char(1) DEFAULT NULL,
  `PED_FORMAPAGTO` char(1) DEFAULT NULL,
  `PED_OBSERVACAO` text
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_pedido`
--

INSERT INTO `tbl_pedido` (`PED_CODIGO`, `CLI_CODIGO`, `PED_DATA`, `PED_HORA`, `PED_BOLETO`, `PED_VALORTOTAL`, `PED_VALORFRETE`, `PED_STATUS`, `PED_FORMAPAGTO`, `PED_OBSERVACAO`) VALUES
(1, 1, '2010-01-16', '19:03:58', NULL, 0.00, 0.00, 'P', 'B', NULL),
(2, 1, '2010-01-18', '13:35:19', NULL, 0.00, 0.00, 'P', 'B', NULL),
(3, 1, '2010-01-18', '13:53:58', NULL, 0.00, 0.00, 'P', 'B', NULL),
(4, 1, '2010-01-20', '09:38:57', NULL, 0.00, 0.00, 'P', 'B', NULL),
(5, 1, '2010-01-20', '10:34:10', NULL, 0.00, 0.00, 'P', 'B', NULL),
(6, 1, '2010-01-20', '12:20:45', NULL, 0.00, 0.00, 'P', 'B', NULL),
(7, 1, '2010-01-20', '12:22:17', NULL, 0.00, 0.00, 'P', 'B', NULL),
(8, 1, '2010-01-20', '12:23:27', NULL, 0.00, 0.00, 'P', 'B', NULL),
(9, 1, '2010-01-20', '12:26:09', NULL, 0.00, 0.00, 'P', 'B', NULL),
(10, 1, '2010-01-20', '12:27:06', NULL, 0.00, 0.00, 'P', 'B', NULL),
(11, 1, '2010-01-20', '12:31:03', NULL, 0.00, 0.00, 'P', 'B', NULL),
(12, 1, '2010-01-20', '12:34:13', NULL, 0.00, 0.00, 'P', 'B', NULL),
(13, 1, '2010-01-20', '12:35:33', NULL, 0.00, 0.00, 'P', 'B', NULL),
(14, 1, '2010-01-20', '12:36:21', NULL, 0.00, 0.00, 'P', 'B', NULL),
(15, 1, '2010-01-20', '14:09:54', NULL, 0.00, 0.00, 'P', 'B', NULL),
(16, 1, '2010-01-20', '15:34:43', NULL, 0.00, 0.00, 'P', 'B', NULL),
(17, 1, '2010-01-20', '15:57:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(18, 1, '2010-01-20', '16:07:38', NULL, 0.00, 0.00, 'P', 'B', NULL),
(19, 1, '2010-01-20', '16:10:11', NULL, 0.00, 0.00, 'P', 'B', NULL),
(20, 1, '2010-01-20', '16:12:06', NULL, 0.00, 0.00, 'P', 'B', NULL),
(21, 1, '2010-01-20', '16:19:17', NULL, 0.00, 0.00, 'P', 'B', NULL),
(22, 1, '2010-01-20', '16:27:13', NULL, 0.00, 0.00, 'P', 'B', NULL),
(23, 2, '2015-08-22', '17:16:36', NULL, 0.00, 0.00, 'P', 'B', NULL),
(24, 2, '2015-08-22', '19:18:34', NULL, 0.00, 0.00, 'P', 'B', NULL),
(25, 1, '2015-08-22', '23:03:41', NULL, 0.00, 0.00, 'P', 'B', NULL),
(26, 2, '2015-08-22', '23:14:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(27, 2, '2015-08-22', '23:21:26', NULL, 0.00, 0.00, 'P', 'B', NULL),
(28, 4, '2015-08-22', '23:28:37', NULL, 0.00, 0.00, 'P', 'B', NULL),
(29, 4, '2015-08-22', '23:48:27', NULL, 0.00, 0.00, 'P', 'B', NULL),
(30, 4, '2015-08-22', '23:53:59', NULL, 0.00, 0.00, 'P', 'B', NULL),
(31, 2, '2015-08-23', '00:14:29', NULL, 0.00, 0.00, 'P', 'B', NULL),
(32, 2, '2015-08-23', '00:17:35', NULL, 0.00, 0.00, 'P', 'B', NULL),
(33, 2, '2015-08-23', '00:24:41', NULL, 0.00, 0.00, 'P', 'B', NULL),
(34, 2, '2015-08-23', '00:28:03', NULL, 0.00, 0.00, 'P', 'B', NULL),
(35, 2, '2015-08-23', '00:55:33', NULL, 0.00, 0.00, 'P', 'B', NULL),
(36, 2, '2015-08-23', '01:15:58', NULL, 0.00, 0.00, 'P', 'B', NULL),
(37, 1, '2015-08-23', '02:08:26', NULL, 0.00, 0.00, 'P', 'B', NULL),
(38, 4, '2015-08-23', '02:09:24', NULL, 0.00, 0.00, 'P', 'B', NULL),
(39, 4, '2015-08-23', '10:45:26', NULL, 0.00, 0.00, 'P', 'B', NULL),
(40, 4, '2015-08-23', '10:54:30', NULL, 0.00, 0.00, 'P', 'B', NULL),
(41, 4, '2015-08-23', '12:00:15', NULL, 0.00, 0.00, 'P', 'B', NULL),
(42, 4, '2015-08-23', '16:46:31', NULL, 0.00, 0.00, 'P', 'B', NULL),
(43, 4, '2015-08-23', '17:06:21', NULL, 0.00, 0.00, 'P', 'B', NULL),
(44, 4, '2015-08-23', '17:07:47', NULL, 0.00, 0.00, 'P', 'B', NULL),
(45, 4, '2015-08-23', '17:08:50', NULL, 0.00, 0.00, 'P', 'B', NULL),
(46, 4, '2015-08-23', '17:31:38', NULL, 0.00, 0.00, 'P', 'B', NULL),
(47, 4, '2015-08-23', '17:40:23', NULL, 0.00, 0.00, 'P', 'B', NULL),
(48, 4, '2015-08-23', '17:46:03', NULL, 0.00, 0.00, 'P', 'B', NULL),
(49, 4, '2015-08-23', '18:23:20', NULL, 0.00, 0.00, 'P', 'B', NULL),
(50, 2, '2015-08-23', '18:56:32', NULL, 0.00, 0.00, 'P', 'B', NULL),
(51, 2, '2015-08-23', '19:03:12', NULL, 0.00, 0.00, 'P', 'B', NULL),
(52, 2, '2015-08-23', '19:50:28', NULL, 0.00, 0.00, 'P', 'B', NULL),
(53, 2, '2015-08-23', '20:44:24', NULL, 0.00, 0.00, 'P', 'B', NULL),
(54, 2, '2015-08-23', '20:50:03', NULL, 0.00, 0.00, 'P', 'B', NULL),
(55, 2, '2015-08-23', '21:01:56', NULL, 0.00, 0.00, 'P', 'B', NULL),
(56, 2, '2015-08-23', '21:07:07', NULL, 0.00, 0.00, 'P', 'B', NULL),
(57, 2, '2015-08-23', '21:36:35', NULL, 0.00, 0.00, 'P', 'B', NULL),
(58, 2, '2015-08-23', '21:46:17', NULL, 0.00, 0.00, 'P', 'B', NULL),
(59, 2, '2015-08-23', '21:48:16', NULL, 0.00, 0.00, 'P', 'B', NULL),
(60, 2, '2015-08-23', '21:50:18', NULL, 0.00, 0.00, 'P', 'B', NULL),
(61, 2, '2015-08-24', '00:21:30', NULL, 0.00, 0.00, 'P', 'B', NULL),
(62, 2, '2015-08-24', '00:22:55', NULL, 0.00, 0.00, 'P', 'B', NULL),
(63, 2, '2015-08-24', '02:10:16', NULL, 0.00, 0.00, 'P', 'B', NULL),
(64, 2, '2015-08-24', '13:47:27', NULL, 0.00, 0.00, 'P', 'B', NULL),
(65, 4, '2015-08-24', '16:39:30', NULL, 0.00, 0.00, 'P', 'B', NULL),
(66, 4, '2015-08-24', '20:48:04', NULL, 0.00, 0.00, 'P', 'B', NULL),
(67, 4, '2015-08-24', '22:13:39', NULL, 0.00, 0.00, 'P', 'B', NULL),
(68, 4, '2015-08-24', '22:40:32', NULL, 0.00, 0.00, 'P', 'B', NULL),
(69, 4, '2015-08-24', '22:53:45', NULL, 0.00, 0.00, 'P', 'B', NULL),
(70, 2, '2015-08-24', '23:43:29', NULL, 0.00, 0.00, 'P', 'B', NULL),
(71, 2, '2015-08-25', '11:02:34', NULL, 0.00, 0.00, 'P', 'B', NULL),
(72, 4, '2015-08-25', '11:06:41', NULL, 0.00, 0.00, 'P', 'B', NULL),
(73, 4, '2015-08-25', '11:13:27', NULL, 0.00, 0.00, 'P', 'B', NULL),
(74, 4, '2015-08-25', '11:14:29', NULL, 0.00, 0.00, 'P', 'B', NULL),
(75, 4, '2015-08-25', '11:17:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(76, 4, '2015-08-25', '11:18:12', NULL, 0.00, 0.00, 'P', 'B', NULL),
(77, 4, '2015-08-25', '11:19:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(78, 2, '2015-08-25', '11:31:31', NULL, 0.00, 0.00, 'P', 'B', NULL),
(79, 4, '2015-08-25', '11:40:01', NULL, 0.00, 0.00, 'P', 'B', NULL),
(80, 2, '2015-08-25', '11:43:04', NULL, 0.00, 0.00, 'P', 'B', NULL),
(81, 2, '2015-08-25', '12:03:11', NULL, 0.00, 0.00, 'P', 'B', NULL),
(82, 2, '2015-08-25', '12:07:08', NULL, 0.00, 0.00, 'P', 'B', NULL),
(83, 4, '2015-08-25', '12:46:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(84, 2, '2015-08-25', '14:01:59', NULL, 0.00, 0.00, 'P', 'B', NULL),
(85, 2, '2015-08-25', '15:19:19', NULL, 0.00, 0.00, 'P', 'B', NULL),
(86, 2, '2015-08-25', '15:48:37', NULL, 0.00, 0.00, 'P', 'B', NULL),
(87, 2, '2015-08-25', '18:07:06', NULL, 0.00, 0.00, 'P', 'B', NULL),
(88, 2, '2015-08-25', '18:23:40', NULL, 0.00, 0.00, 'P', 'B', NULL),
(89, 2, '2015-08-25', '18:26:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(90, 2, '2015-08-25', '19:58:40', NULL, 0.00, 0.00, 'P', 'B', NULL),
(91, 2, '2015-08-25', '20:00:10', NULL, 0.00, 0.00, 'P', 'B', NULL),
(92, 2, '2015-08-28', '14:50:59', NULL, 0.00, 0.00, 'P', 'B', NULL),
(93, 2, '2015-08-28', '15:54:39', NULL, 0.00, 0.00, 'P', 'B', NULL),
(94, 2, '2015-08-29', '11:23:26', NULL, 0.00, 0.00, 'P', 'B', NULL),
(95, 2, '2015-08-29', '11:24:58', NULL, 0.00, 0.00, 'P', 'B', NULL),
(96, 2, '2015-08-29', '11:26:19', NULL, 0.00, 0.00, 'P', 'B', NULL),
(97, 2, '2015-08-29', '15:29:54', NULL, 0.00, 0.00, 'P', 'B', NULL),
(98, 2, '2015-08-31', '00:36:05', NULL, 0.00, 0.00, 'P', 'B', NULL),
(99, 2, '2015-09-02', '13:35:33', NULL, 0.00, 0.00, 'P', 'B', NULL),
(100, 2, '2015-09-02', '14:21:09', NULL, 0.00, 0.00, 'P', 'B', NULL),
(101, 2, '2015-09-02', '14:34:45', NULL, 0.00, 0.00, 'P', 'B', NULL),
(102, 2, '2015-09-02', '14:43:31', NULL, 0.00, 0.00, 'P', 'B', NULL),
(103, 2, '2015-09-02', '14:45:49', NULL, 0.00, 0.00, 'P', 'B', NULL),
(104, 2, '2015-09-02', '14:53:44', NULL, 0.00, 0.00, 'P', 'B', NULL),
(105, 2, '2015-09-02', '18:06:45', NULL, 0.00, 0.00, 'P', 'B', NULL),
(106, 2, '2015-09-03', '15:10:19', NULL, 0.00, 0.00, 'P', 'B', NULL),
(107, 2, '2015-09-11', '01:57:03', NULL, 0.00, 0.00, 'P', 'B', NULL),
(108, 2, '2015-09-11', '10:56:11', NULL, 0.00, 0.00, 'P', 'B', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_produto`
--

CREATE TABLE IF NOT EXISTS `tbl_produto` (
  `PROD_CODIGO` int(11) NOT NULL,
  `FOR_CODIGO` int(11) NOT NULL,
  `CAT_CODIGO` int(11) NOT NULL,
  `PROD_DESCRICAO` varchar(40) NOT NULL,
  `PROD_VALOR` decimal(10,2) DEFAULT NULL,
  `PROD_QUANTIDADE` decimal(10,2) DEFAULT NULL,
  `PROD_TIPO` varchar(5) DEFAULT NULL,
  `PROD_OBS` text
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_produto`
--

INSERT INTO `tbl_produto` (`PROD_CODIGO`, `FOR_CODIGO`, `CAT_CODIGO`, `PROD_DESCRICAO`, `PROD_VALOR`, `PROD_QUANTIDADE`, `PROD_TIPO`, `PROD_OBS`) VALUES
(4, 3, 2, 'Java I SE - Sistema de Estoque', 99.00, 50.00, 'unid', 'Java I SE - Sistema de Controle de Estoque'),
(2, 3, 2, 'Java II SE O.O. 200 videos', 109.00, 50.00, 'unid', 'Aprenda OrientaÃ§Ã£o a objetos com java'),
(5, 4, 4, 'Delphi I - Estoque', 99.00, 800.00, 'unid', 'Primeiro Curso de Delphi, em que foi mostrado como criar um sistema de controle de estoque!'),
(6, 3, 3, 'Curso de PHP com Mysql e Dreamweaver cs4', 99.00, 800.00, 'unid', 'primeiro curso de php feito pelo Nerizon'),
(7, 3, 4, 'Delphi II AvanÃ§ado - 6.6 giga', 109.00, 900.00, 'unid', 'Depois do grande sucesso que meu primeiro curso de Delphi fez, acabei lanÃ§ando este segundo curso, Delphi 7 avanÃ§ado, e ainda mostro tambÃ©m como instalar e criar aplicativos usando o Delphi 2007'),
(8, 3, 4, 'Delphi III Biometria O.O. XML Criptograf', 139.00, 900.00, 'unid', 'LanÃ§amento 24/01/2008 - Meu 3Âº e mais trabalhoso curso de Delphi 7 AvanÃ§ado contendo:  (Biometria (impressÃ£o digital), XML, Criptografia, Report Builder, Rave report, Fast Report, OrientaÃ§Ã£o a Objetos, CriaÃ§Ã£o de componentes, conexÃµes com Oracle, SQL Server, MySQL, PostgreSQL e Firebird (usando componentes Zeos, Ado, DBExpress e BDE), controle de login, tabelas virtuais.'),
(9, 3, 2, 'Java III - EE WEB', 139.00, 900.00, 'unid', 'J2EE'),
(10, 3, 2, 'Java IV ME - celular', 99.00, 900.00, 'unid', 'SÃ£o mais de 100 vÃ­deo aulas, aprenda: Instalar, configurar, criar aplicativos diversos, menus, copiar para o celular (e rodar nele). VocÃª vai aprender a criar diversos aplicativos para rodar no celular. TambÃ©m verÃ¡ como criar um sistema que armazene e manipule os dados na memÃ³ria do celular (RMS).'),
(11, 3, 2, 'Java V - ME Celuar com banco de Dados', 99.00, 900.00, 'unid', ''),
(12, 3, 2, 'Java VI Hibernate ', 119.00, 900.00, 'unid', 'O Hibernate Ã© um framework para o mapeamento objeto-relacional, ele facilita o mapeamento dos atributos entre uma base tradicional de dados relacionais e o modelo objeto de uma aplicaÃ§Ã£o, com isso, diminui a complexidade de criar programas em java, agilizando assim o desenvolvimento.'),
(13, 3, 5, 'SQL + Banco + Modelagem', 119.00, 900.00, 'unid', ''),
(14, 3, 5, 'Oracle pl/sql Forms Reports', 129.00, 900.00, 'unid', 'ORACLE PL/SQL Procedures Functions Triggers Oracle Forms e Reports, custo R$129,00\r\nlanÃ§ado em 14/07/2009'),
(15, 3, 6, 'Flex 164 videos 3 dvd', 114.00, 900.00, 'unid', '3dvd e 164 VÃ­deo Aulas de Flex, usado para criar aplicaÃ§Ãµes ricas para internet. Uma das mais valorizadas linguagens de programaÃ§Ã£o da atualidade. O Flex Ã© uma estrutura de cÃ³digo aberto altamente produtiva e gratuita para a criaÃ§Ã£o e manutenÃ§Ã£o de aplicativos Web!'),
(16, 3, 6, 'Flex com Java BlazeDS e Banco ', 114.00, 900.00, 'unid', '7.5 Gibagyte e 2 dvd em 84 VÃ­deo Aulas de Flex com Java e BlazeDS e Banco de Dados e RelatÃ³rio. O Flex Ã© uma estrutura de cÃ³digo aberto altamente produtiva e gratuita para a criaÃ§Ã£o e manutenÃ§Ã£o de aplicativos Web!'),
(17, 3, 7, 'Algoritmos e LÃ³gica Pascal Java e C', 99.00, 900.00, 'unid', 'Agora sim, vocÃª que quer iniciar nesse fantÃ¡stico mundo da programaÃ§Ã£o, entÃ£o comece por esse curso. \r\nSe vocÃª acha que entende bem de algoritmo e lÃ³gica, mas tem dificuldade de implementar no Java ou Pascal no no C,\r\nesse curso tambÃ©m lhe serÃ¡ muito Ãºtil. Por fim, com certeza vocÃª conhece alguÃ©m que tem dificuldade em aprender programar, \r\nentÃ£o indique esse curso para ele(a).'),
(21, 3, 32, 'san', 9.00, 9.00, 'unid', 'hgfgjfjfjgf');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_sub_menu_horizontal`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_menu_horizontal` (
  `SUBMENUHOR_CODIGO` int(11) NOT NULL,
  `MENUHOR_CODIGO` int(11) NOT NULL,
  `SUBMENUHOR_DESCRICAO` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_sub_menu_horizontal`
--

INSERT INTO `tbl_sub_menu_horizontal` (`SUBMENUHOR_CODIGO`, `MENUHOR_CODIGO`, `SUBMENUHOR_DESCRICAO`) VALUES
(4, 1, 'Css Base'),
(5, 1, 'componentes'),
(6, 1, 'Javascript');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_sub_menu_vertical`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_menu_vertical` (
  `SUBCAT_CODIGO` int(11) NOT NULL,
  `CAT_CODIGO` int(11) NOT NULL,
  `SUBCAT_DESCRICAO` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_sub_menu_vertical`
--

INSERT INTO `tbl_sub_menu_vertical` (`SUBCAT_CODIGO`, `CAT_CODIGO`, `SUBCAT_DESCRICAO`) VALUES
(26, 28, 'ito'),
(7, 7, 'JavaEE'),
(5, 7, 'linguangem C'),
(10, 7, 'linguegem php'),
(21, 5, 'Mysql'),
(22, 5, 'postegreSQL'),
(24, 7, 'linguagem c++'),
(28, 32, 'santos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `USU_CODIGO` int(11) NOT NULL,
  `USU_NOME` varchar(40) NOT NULL,
  `USU_LOGIN` varchar(10) NOT NULL,
  `USU_SENHA` text NOT NULL,
  `USU_NIVEL` char(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`USU_CODIGO`, `USU_NOME`, `USU_LOGIN`, `USU_SENHA`, `USU_NIVEL`) VALUES
(9, 'jailton', 'ito', '202cb962ac59075b964b07152d234b70', 'A'),
(8, 'jai', 'jai', 'MTIz', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_carrinho`
--
ALTER TABLE `tbl_carrinho`
  ADD PRIMARY KEY (`PROD_CODIGO`,`CAR_SESSAO`);

--
-- Indexes for table `tbl_cidade`
--
ALTER TABLE `tbl_cidade`
  ADD PRIMARY KEY (`CID_CODIGO`);

--
-- Indexes for table `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  ADD PRIMARY KEY (`CLI_CODIGO`),
  ADD KEY `FK_CIDADES_CLIENTES` (`CID_CODIGO`);

--
-- Indexes for table `tbl_fornecedor`
--
ALTER TABLE `tbl_fornecedor`
  ADD PRIMARY KEY (`FOR_CODIGO`),
  ADD KEY `FK_CIDADE_FORNECEDOR` (`CID_CODIGO`);

--
-- Indexes for table `tbl_imagem`
--
ALTER TABLE `tbl_imagem`
  ADD PRIMARY KEY (`IMG_CODIGO`),
  ADD KEY `FK_IMAGENS_DO_PRODUTO` (`PROD_CODIGO`);

--
-- Indexes for table `tbl_itens_pedido`
--
ALTER TABLE `tbl_itens_pedido`
  ADD PRIMARY KEY (`PROD_CODIGO`,`PED_CODIGO`),
  ADD KEY `FK_TBL_ITENS_PEDIDO2` (`PED_CODIGO`);

--
-- Indexes for table `tbl_menu_horizontal`
--
ALTER TABLE `tbl_menu_horizontal`
  ADD PRIMARY KEY (`MENUHOR_CODIGO`);

--
-- Indexes for table `tbl_menu_vertical`
--
ALTER TABLE `tbl_menu_vertical`
  ADD PRIMARY KEY (`CAT_CODIGO`);

--
-- Indexes for table `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD PRIMARY KEY (`PED_CODIGO`),
  ADD KEY `FK_PEDIDO_CLIENTE` (`CLI_CODIGO`);

--
-- Indexes for table `tbl_produto`
--
ALTER TABLE `tbl_produto`
  ADD PRIMARY KEY (`PROD_CODIGO`),
  ADD KEY `FK_PRODUTO_CATEGORIA` (`CAT_CODIGO`),
  ADD KEY `FK_PRODUTO_FORNECEDOR` (`FOR_CODIGO`);

--
-- Indexes for table `tbl_sub_menu_horizontal`
--
ALTER TABLE `tbl_sub_menu_horizontal`
  ADD PRIMARY KEY (`SUBMENUHOR_CODIGO`),
  ADD KEY `FK_SUBMENU_HORIZONTAL` (`MENUHOR_CODIGO`);

--
-- Indexes for table `tbl_sub_menu_vertical`
--
ALTER TABLE `tbl_sub_menu_vertical`
  ADD PRIMARY KEY (`SUBCAT_CODIGO`),
  ADD KEY `FK_SUB_CAT_DA_CATEGORIA` (`CAT_CODIGO`);

--
-- Indexes for table `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`USU_CODIGO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cidade`
--
ALTER TABLE `tbl_cidade`
  MODIFY `CID_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  MODIFY `CLI_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_fornecedor`
--
ALTER TABLE `tbl_fornecedor`
  MODIFY `FOR_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_imagem`
--
ALTER TABLE `tbl_imagem`
  MODIFY `IMG_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `tbl_menu_horizontal`
--
ALTER TABLE `tbl_menu_horizontal`
  MODIFY `MENUHOR_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_menu_vertical`
--
ALTER TABLE `tbl_menu_vertical`
  MODIFY `CAT_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  MODIFY `PED_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `tbl_produto`
--
ALTER TABLE `tbl_produto`
  MODIFY `PROD_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tbl_sub_menu_horizontal`
--
ALTER TABLE `tbl_sub_menu_horizontal`
  MODIFY `SUBMENUHOR_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_sub_menu_vertical`
--
ALTER TABLE `tbl_sub_menu_vertical`
  MODIFY `SUBCAT_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `USU_CODIGO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

function callBackMudancasStatus(response){
	if(response.status==='connected'){
		$('#status').html('<a href="javascript:void(0);" id="logout" onclick="logOut();">Sair</a>');
		$('#status').click(function() { location.reload(); });
		testAPI();
	}
	else if(response.status==='not_authorized'){
		login();
	}
}

window.fbAsyncInit = function(){
	FB.init({
		appId: '157346631276525',
		cookie: true,
		xfbml: true,
		version: 'v2.4'
	});
	FB.getLoginStatus(function(response){
		callBackMudancasStatus(response);
	});
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

function testAPI(){
	FB.api('/me',function(response){
		$('#usuario').append('<p id="inicial">Ola, '+response.name+' Seja Bem Vindo!</p>');
	});
}
function logOut(){
	FB.logout(function(response){
		callBackMudancasStatus(response);
	});
}
function login(){
	FB.login(function(response){
		callBackMudancasStatus(response);
	}); 
}
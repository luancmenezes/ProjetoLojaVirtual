<!DOCTYPE html>
<html>
	<head>
    <?php require('util/head.php');?>
  </head>
	<body>

<!-- ////////////////////////CABECALHO//////////////////////////////////////////////////// -->  

<div class="container">
  <div class="row">
    <header class="cabecalho span12">
      <h1 align="center">Anuncie Aqui</h1>
    </header>
  </div>
</div>
		
<!-- //////////////////MENU HORIZONTAL////////////////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <nav class="navbar span12 MenuHorizontal">
      <div class="container">
        <button class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="nav-collapse collapse">
          <nav id="menu_horizontal">
            <ul class="nav nav-pills">
              <li class="dropdown"><a href="#">Divulgue sua Empresa</a></li>
              <li class="dropdown" id="status"><a href="login.php#myModal" role="button" data-toggle="modal">Efetuar Login</a></li>
              <li class="dropdown"><div id="usuario"></div></li>
            </ul>
          </nav>
          <?php require('login.php');?>
        </div>
      </div>
    </nav>
  </div>
</div>

<!-- /////////////////////////CORPO DA PAGINA- CENTRO////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <section class="span12" id="btn-dropdown">
      <div id="myCarousel" class="carousel slide">
        <div class="carousel-inner">
        <div class="active item"><img src="imagens/flash.jpg" alt="imagens 1" width="100px" height="100px" ></div>
        <div class="item"><img src="imagens/fireworks5.JPG" alt="imagens 2" width="100px" height="100px"></div>
        <div class="item"><img src="imagens/php.gif" alt="imagens 3" width="100px" height="100px"></div>
      </div>
      <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
      </div>
    </section>
  </div>
</div>

<!-- //////////////////////////RODAPE////////////////////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <footer class="footer span12">
      <h3 align="center">INFORMACOES EXTRAS DO SITE</h3>
    </footer>
  </div>
</div>

</body>
</html>

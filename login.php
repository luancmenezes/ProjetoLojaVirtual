<div id="myModal" class="modal hide fade login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 id="myModalLabel">Entrar</h3>
  </div>
  <div class="modal-body">
  <a href="#" onclick="login();" data-dismiss="modal">Login com o Facebook</a>
    <form class="form-horizontal" method="post" action="admin/login/login_validacao.php">
      <div class="control-group">
        <label class="control-label" for="inputEmail">Login</label>
        <div class="controls">
          <input type="text" name="usu_login" class="input-small" placeholder="Login">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="inputPassword">Senha</label>
        <div class="controls">
          <input type="password" name="usu_senha" class="input-small" placeholder="Senha">
        </div>
      </div>

      <div class="control-group">
        <div class="controls">
          <button type="submit" class="btn">Entrar</button>
        </div>
      </div>

      <div class="modal-footer">
        <p><a href="#">cadastre-se aqui</a></p>
      </div>
    </form>                    
  </div>
</div>
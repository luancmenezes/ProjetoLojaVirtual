<?php $filtro = $_REQUEST['pesquisa'];?>
<table class="table table-bordered table-hover table-condensed" cellspacing="2">
  <tr class="titulos_lista_pesquisa">
    <td colspan="4" align="center"><h3 align="center"> Lista de Cidades</h3>
      <form class="form-search" method="post" action="index.php?tabela=cidade&acao=listar">
        <div class="input-append">
          <input name="pesquisa" type="text" class="span2 search-query" value="<?php echo $filtro; ?>">
          <button type="submit" class="btn">Busca</button>
        </div>
      </form>
    </td>
  </tr>
  <tr class="info">
    <td><a href="index.php?tabela=cidade&acao=listar&ordem=CID_DESCRICAO">Descrição</a></td>
    <td><a href="index.php?tabela=cidade&acao=listar&ordem=CID_UF">UF</a></td>
    <td colspan="2"><a href="index.php?tabela=cidade&acao=incluir">Novo Registro</a></td>
  </tr>
  <?php

	while($oquefazer->registros = $oquefazer->resultado->FetchNextObject()){?>
    <tr>
      <td><?php echo $oquefazer->registros->CID_DESCRICAO;?></td>
      <td><?php echo $oquefazer->registros->CID_UF;?></td>
      <td><a href="index.php?tabela=cidade&acao=alterar&codigo=<?php echo $oquefazer->registros->CID_CODIGO;?>">Alterar</a></td>
      <td><a href="javascript:if(confirm('Deseja excluir esse registro ?')) {location='index.php?tabela=cidade&acao=excluir&codigo=<?php echo $oquefazer->registros->CID_CODIGO;?>';}">Excluir</a></td>
    </tr>
	<?php } ?>         
	<tr class="error">
		<?php
		if ($oquefazer->total_registros()==0){?>
			<td colspan="4"><p><?php echo "Nenhum registro encontrado para palavra "."'$filtro'";?></p></td>
		<?php }
		else{
		?>
			<td colspan="4"><p>Numero de registros: <?php echo $oquefazer->total_registros();?></p></td>
		<?php } ?>
	</tr>
</table>
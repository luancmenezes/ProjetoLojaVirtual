<?php

     require('entidadeDAO/usuario_DAO.php');
	 
	 $oquefazer = new usuario_DAO();
	 
	 $acao = $_REQUEST['acao'];
	 
	 if($acao == 'listar')
	 {
    	$filtro = $_REQUEST['pesquisa'];
        $oquefazer->listar_usuario();
		require('usuario/usuario_lista.php');
	 }

	 if($acao == 'excluir')
	 {
		$oquefazer->excluir();
		$oquefazer->listar_usuario();
		require('usuario/usuario_lista.php');
	 }
	 
	 if($acao == 'incluir')
	 {
		require('usuario/usuario_form.php');
	 }

	 if($acao == 'gravar_incluir')
	 {
		$oquefazer->gravar_incluir();
		$oquefazer->listar_usuario();
		require('usuario/usuario_lista.php');
	 }

	 if($acao == 'alterar')
	 {
		$oquefazer->alterar();
		require('usuario/usuario_form.php');
	 }

	 if($acao == 'gravar_alterar')
	 {
		$oquefazer->gravar_alterar();
		$oquefazer->listar_usuario();
		require('usuario/usuario_lista.php');
	 }

	
?>
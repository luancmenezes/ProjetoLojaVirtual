<?php

     require('entidadeDAO/produtos_DAO.php');
	 
	 $oquefazer = new produto_DAO();
	 
	 $acao = $_REQUEST['acao'];
	 
	 if($acao == 'listar')
	 {
    	$filtro = $_REQUEST['pesquisa'];
        $oquefazer->listar_produto();
		require('produto/produtos_lista.php');
	 }

	 if($acao == 'excluir')
	 {
		$oquefazer->excluir();
		$oquefazer->listar_produto();
		require('produto/produtos_lista.php');
	 }
	 
	 if($acao == 'incluir')
	 {
		require('produto/produtos_form.php');
	 }

	 if($acao == 'gravar_incluir')
	 {
		$oquefazer->gravar_incluir();
		$oquefazer->listar_produto();
		require('produto/produtos_lista.php');
	 }

	 if($acao == 'alterar')
	 {
		$oquefazer->alterar();
		require('produto/produtos_form.php');
	 }

	 if($acao == 'gravar_alterar')
	 {
		$oquefazer->gravar_alterar();
		$oquefazer->listar_produto();
		require('produto/produtos_lista.php');
	 }

	
?>
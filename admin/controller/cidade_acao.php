<?php

     require('entidadeDAO/cidade_DAO.php');
	 
	 $oquefazer = new cidade_DAO();
	 
	 $acao = $_REQUEST['acao'];

	 
	 if($acao == 'listar')
	 {
    	 $filtro = $_REQUEST['pesquisa'];
        $oquefazer->listar_cidade();
		require('cidade/cidade_lista.php');
	 }

	 if($acao == 'excluir')
	 {
		$oquefazer->excluir();
		$oquefazer->listar_cidade();
		require('cidade/cidade_lista.php');
		alerta("O registro foi excluido com sucesso");	   
	 }
	 
	 if($acao == 'incluir')
	 {
		require('cidade/cidade_form.php');
	 }

	 if($acao == 'gravar_incluir')
	 {
		$oquefazer->gravar_incluir();
		$oquefazer->listar_cidade();
		require('cidade/cidade_lista.php');
		alerta("O registro foi incluido com sucesso");
	 }

	 if($acao == 'alterar')
	 {
		$oquefazer->alterar();
		require('cidade/cidade_form.php');
	 }

	 if($acao == 'gravar_alterar')
	 {
		$oquefazer->gravar_alterar();
		$oquefazer->listar_cidade();
		require('cidade/cidade_lista.php');
		alerta("O registro foi alterado com sucesso");
	 }	
?>
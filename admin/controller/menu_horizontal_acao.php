<?php

	 session_start();
	 
     require('entidadeDAO/menu_horizontal_DAO.php');
	 
	 $oquefazer = new menu_horizontal_DAO();
	 
	 $acao = $_REQUEST['acao'];
	 
	 if($acao == 'listar')
	 {
    	 $filtro = $_REQUEST['pesquisa'];
        $oquefazer->listar_menu_horizontal();
		require('menu_horizontal/menu_horizontal_lista.php');
	 }

	 if($acao == 'excluir')
	 {
		$oquefazer->excluir();
		$oquefazer->listar_menu_horizontal();
		require('menu_horizontal/menu_horizontal_lista.php');
	 }
	 
	 if($acao == 'incluir')
	 {
		require('menu_horizontal/menu_horizontal_form.php');
	 }

	 if($acao == 'gravar_incluir')
	 {

		$oquefazer->gravar_incluir($_SESSION['sessao_codigo_usuario']);
		$oquefazer->listar_menu_horizontal();
		require('menu_horizontal/menu_horizontal_lista.php');
	 }

	 if($acao == 'alterar')
	 {
		$oquefazer->alterar();
		require('menu_horizontal/menu_horizontal_form.php');
	 }

	 if($acao == 'gravar_alterar')
	 {
		$oquefazer->gravar_alterar();
		$oquefazer->listar_menu_horizontal();
		require('menu_horizontal/menu_horizontal_lista.php');
	 }
?>
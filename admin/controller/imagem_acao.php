<?php

     require('entidadeDAO/imagem_DAO.php');
	 
	 $oquefazer = new imagem_DAO();
	 
	 $acao = $_REQUEST['acao'];
	 
	 if($acao == 'listar')
	 {
    	$filtro = $_REQUEST['pesquisa'];
        $oquefazer->listar_imagem();
		require('imagem/imagem_lista.php');
	 }

	 if($acao == 'excluir')
	 {
		$oquefazer->excluir();
		$oquefazer->listar_imagem();
		require('imagem/imagem_lista.php');
	 }
	 
	 if($acao == 'incluir')
	 {
		require('imagem/imagem_form.php');
	 }

	 if($acao == 'gravar_incluir')
	 {
		$oquefazer->gravar_incluir();
		$oquefazer->listar_imagem();
		require('imagem/imagem_lista.php');
	 }

	 if($acao == 'alterar')
	 {
		$oquefazer->alterar();
		require('imagem/imagem_form.php');
	 }

	 if($acao == 'gravar_alterar')
	 {
		$oquefazer->gravar_alterar();
		$oquefazer->listar_imagem();
		require('imagem/imagem_lista.php');
	 }

	
?>
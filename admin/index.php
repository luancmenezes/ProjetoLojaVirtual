<?php
     session_start();     
	 if(!$_SESSION['sessao_codigo_usuario']){
		 require('../util/funcoes.php');
		 direciona('../index.php');
		 exit;
	 }
	 else{ 
    $tabela = $_REQUEST['tabela'];
    $acao   = $_REQUEST['acao'];
    $desc   = $_REQUEST['desc'];  
	 ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta name="description" content="adminstracão"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>adminstracão</title>
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" href="../bootstrap/css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="../css/menu_vertical.css" />
  <link href="../css/estilos.css" rel="stylesheet" type="text/css" />
  <!-- <script type="text/javascript" src="../js/funcoes.js"></script> -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-1.7.1.min.js"><\/script>')</script>
  <script type="text/javascript" src="../bootstrap/js/bootstrap.js"></script>
</head>  
<body>

<div class="container">
  <div class="row">
    <header class="cabecalho span12">
      <h1 align="center">ADMINISTRACÃO</h1>
    </header>
  </div>
</div>

<div class="container">
  <div class="row">
    <section class="span2 ladoesquerdo">
      <nav id="menu_vertical">
        <ul class="nav nav-pills nav-stacked">
          <li><a href="index.php?&desc=Home">Home</a></li>
          <li><a href="index.php?tabela=menu_vertical&acao=listar&desc=Menu Vertical">Menu Vertical</a></li>
          <li><a href="index.php?tabela=menu_horizontal&acao=listar&desc=Menu Horizontal">Menu Horizontal</a></li>
          <li><a href="index.php?tabela=cidade&acao=listar&desc=Cidade">Cidade</a></li>
          <li><a>Clientes</a></li>
          <li><a href="index.php?tabela=fornecedor&acao=listar&desc=Fornecedor">Fornecedor</a></li>
          <li><a>Pedidos</a></li>
          <li><a href="index.php?tabela=produto&acao=listar&desc=Produtos">Produtos</a></li>
          <li><a>Promocao</a></li>
          <li><a href="index.php?tabela=usuario&acao=listar&desc=Usuarios">Usuario</a></li>
          <li><a href="../home.php">voltar</a></li>
        </ul>
      </nav>
    </section>

<!-- /////////////////////////CORPO DA PAGINA- CENTRO////////////////////////////////////// -->

    <section class="span10 centro" id="btn-dropdown">
      <?php
      require('../util/conecta.php');                       
      if($tabela == "cidade")
       require('controller/cidade_acao.php');
      else if($tabela == "menu_vertical")
       require('controller/menu_vertical_acao.php'); 
      else if($tabela == "usuario")
       require('controller/usuario_acao.php'); 
      else if($tabela == "fornecedor")
       require('controller/fornecedor_acao.php'); 
      else if($tabela == "produto")
       require('controller/produtos_acao.php'); 
      else if($tabela == "imagem")
       require('controller/imagem_acao.php');
      else if($tabela == "sub_menu_vertical")
       require('controller/sub_menu_vertical_acao.php');
      else if($tabela == "menu_horizontal")
       require('controller/menu_horizontal_acao.php');
      else if($tabela == "sub_menu_horizontal")
       require('controller/sub_menu_horizontal_acao.php');
      else{
       require('principal.php');      
      }?>
    </section>
  </div>
</div>

<!-- //////////////////////////RODAPE////////////////////////////////////////////////////// -->

<div class="container">
  <div class="row">
    <footer class="footer span12">
      <h3 align="center">INFORMACOES EXTRAS DA EMPRESA</h3>
    </footer>
  </div>
</div>

<!-- ////////////////////////////////////////////////////////////////////////////////////// -->

  </body>
</html>     
<?php } ?>
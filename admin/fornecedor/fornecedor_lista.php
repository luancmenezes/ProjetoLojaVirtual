<table class="table table-bordered table-hover table-condensed" cellspacing="2">
  <tr>
    <td colspan="5" align="center"><h2 align="center"> Lista de Fornecedores</h2> 
    <form class="form-search" method="post" action="index.php?tabela=fornecedor&acao=listar">
      <div class="input-append">
        <input name="pesquisa" type="text" class="span2 search-query" value="<?php echo $filtro; ?>">
        <button type="submit" class="btn">Busca</button>
      </div>
    </form>
  </td>
  </tr>
  <tr class="info">
    <td><a href="index.php?tabela=fornecedor&acao=listar&ordem=FOR_RAZAOSOCIAL">Razao Social </td>
    <td><a href="index.php?tabela=fornecedor&acao=listar&ordem=FOR_NOME_FANTASIA">Nome Fantasia</td>
    <td><a href="index.php?tabela=fornecedor&acao=listar&ordem=FOR_CNPJ">CNPJ</td>
    <td colspan="2"><a href="index.php?tabela=fornecedor&acao=incluir">Novo Registro</a></td>
  </tr>
  <?php
	while($oquefazer->registros = $oquefazer->resultado->FetchNextObject()){?>
    <tr>
      <td><?php echo $oquefazer->registros->FOR_RAZAOSOCIAL;?></td>
      <td><?php echo $oquefazer->registros->FOR_NOME_FANTASIA;?></td>
      <td><?php echo $oquefazer->registros->FOR_CNPJ;?></td>
      <td><a href="index.php?tabela=fornecedor&acao=alterar&codigo=<?php echo $oquefazer->registros->FOR_CODIGO;?>">Alterar</a></td>
      <td><a href="javascript:if(confirm('Deseja excluir esse registro ?')) {location='index.php?tabela=fornecedor&acao=excluir&codigo=<?php echo $oquefazer->registros->FOR_CODIGO;?>';}">Excluir</a></td>
    </tr>
<?php 
	} ?>         
  <tr class="error">
    <td colspan="5"><p>Numero de registros: <?php echo $oquefazer->total_registros();?></p></td>
  </tr>
</table>
